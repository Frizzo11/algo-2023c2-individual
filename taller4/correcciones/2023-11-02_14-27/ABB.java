package aed;

import java.util.*;

public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private class Nodo {
        T valor;
        Nodo izquierdo;
        Nodo derecho;

        Nodo(T valor) {
            this.valor = valor;
            this.izquierdo = null;
            this.derecho = null;
        }
    }

    private Nodo raiz;
    private int cardinalidad;

    public ABB() {
        raiz = null;
        cardinalidad = 0;
    }

    public int cardinal() {
        return cardinalidad;
    }

    public T minimo() {
        if (raiz == null) {
            throw new NoSuchElementException("El árbol está vacío");
        }
        Nodo nodoActual = raiz;
        while (nodoActual.izquierdo != null) {
            nodoActual = nodoActual.izquierdo;
        }
        return nodoActual.valor;
    }

    public T maximo() {
        if (raiz == null) {
            throw new NoSuchElementException("El árbol está vacío");
        }
        Nodo nodoActual = raiz;
        while (nodoActual.derecho != null) {
            nodoActual = nodoActual.derecho;
        }
        return nodoActual.valor;
    }

    public void insertar(T elem) {
        if (!pertenece(elem)) {
            raiz = insertarRecursivo(raiz, elem);
            cardinalidad++;
        }
    }

    private Nodo insertarRecursivo(Nodo nodo, T elem) {
        if (nodo == null) {
            return new Nodo(elem);
        }

        if (elem.compareTo(nodo.valor) < 0) {
            nodo.izquierdo = insertarRecursivo(nodo.izquierdo, elem);
        } else if (elem.compareTo(nodo.valor) > 0) {
            nodo.derecho = insertarRecursivo(nodo.derecho, elem);
        }

        return nodo;
    }

    public boolean pertenece(T elem) {
        return perteneceRecursivo(raiz, elem);
    }

    private boolean perteneceRecursivo(Nodo nodo, T elem) {
        if (nodo == null) {
            return false;
        }

        if (elem.equals(nodo.valor)) {
            return true;
        }

        if (elem.compareTo(nodo.valor) < 0) {
            return perteneceRecursivo(nodo.izquierdo, elem);
        } else {
            return perteneceRecursivo(nodo.derecho, elem);
        }
    }

    public void eliminar(T elem) {
        if (pertenece(elem)) {
            raiz = eliminarRecursivo(raiz, elem);
            cardinalidad--;
        }
    }

    private Nodo eliminarRecursivo(Nodo nodo, T elem) {
        if (nodo == null) {
            return nodo;
        }

        if (elem.compareTo(nodo.valor) < 0) {
            nodo.izquierdo = eliminarRecursivo(nodo.izquierdo, elem);
        } else if (elem.compareTo(nodo.valor) > 0) {
            nodo.derecho = eliminarRecursivo(nodo.derecho, elem);
        } else {
            if (nodo.izquierdo == null) {
                return nodo.derecho;
            } else if (nodo.derecho == null) {
                return nodo.izquierdo;
            }

            nodo.valor = minimoValor(nodo.derecho);
            nodo.derecho = eliminarRecursivo(nodo.derecho, nodo.valor);
        }

        return nodo;
    }

    private T minimoValor(Nodo nodo) {
        T minValor = nodo.valor;
        while (nodo.izquierdo != null) {
            minValor = nodo.izquierdo.valor;
            nodo = nodo.izquierdo;
        }
        return minValor;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (raiz != null) {
            toStringRecursivo(raiz, sb);
        }
        sb.append("}");
        return sb.toString();
    }
    
    private void toStringRecursivo(Nodo nodo, StringBuilder sb) {
        if (nodo != null) {
            if (nodo.izquierdo != null) {
                toStringRecursivo(nodo.izquierdo, sb);
                sb.append(",");
            }
            sb.append(nodo.valor.toString());
            if (nodo.derecho != null) {
                sb.append(",");
                toStringRecursivo(nodo.derecho, sb);
            }
        }
    }

    private class ABB_Iterador implements Iterador<T> {
        private Stack<Nodo> pila;

        ABB_Iterador() {
            pila = new Stack<>();
            Nodo nodo = raiz;
            while (nodo != null) {
                pila.push(nodo);
                nodo = nodo.izquierdo;
            }
        }

        public boolean haySiguiente() {
            return !pila.isEmpty();
        }

        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay más elementos en el árbol");
            }

            Nodo actual = pila.pop();
            T valor = actual.valor;

            if (actual.derecho != null) {
                Nodo nodo = actual.derecho;
                while (nodo != null) {
                    pila.push(nodo);
                    nodo = nodo.izquierdo;
                }
            }

            return valor;
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }
}

