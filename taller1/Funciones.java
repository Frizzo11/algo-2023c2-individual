package aed;

class Funciones {
    
    int cuadrado(int x) {
        int res;
        res = x*x;
        return res;
    }

    double distancia(double x, double y){
        double res;
        res = Math.sqrt(x*x+y*y);
        return res;
    }

    boolean esPar(int n) {
        
        boolean res = (n%2==0);
        return res;
    }

    boolean esBisiesto(int n) {
        if ((n % 4 == 0 && n % 100 != 0)|| n % 400 == 0){
            return true; 
        }
        else{
            return false;
        }
    }

    int factorialIterativo(int n) {
        int i=1;
        int res=1;
        while(i<=n){
            res=res*i;
            i++;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res = 0;
        if (n>=1){
            res=n*factorialRecursivo(n-1);
        }
        if (res == 0){
            res = 1;
            return res;
        }
        else {
            return res;
        }
    }
    
    boolean esPrimo(int n) {
        boolean res=false;
        int i = 0;
        int divs = 0;
        while(i<n){
            if (n % (n-i) == 0){
                divs++;
            }
            i++;
        }
        if (divs==2){
            res = true;
        }
        return res;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        int x = 0;
        if (numeros.length == 0){
            return 0;
        }
        else{
            while(x < numeros.length){
                res=res+numeros[x];
                x++;
            }
            return res;
        }
    }

    int busqueda(int[] numeros, int buscado) {
        int i = 0;
        int res = 0;
        while(i<numeros.length){
            if (numeros[i]==buscado){
                res = i;
            }
            i++;
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        boolean res = false;
        for(int x:numeros){
            if (esPrimo(x)==true){
                res = true;
            }
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        boolean res=true;
        for(int x:numeros){
            if(esPar(x)==false){
                res=false;
            }
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean res=true;
        if(s1.length()>s2.length()){
            res = false;
        }
        else{
            for(int x1=0;x1<s1.length();x1++){
                if (s1.charAt(x1)!=s2.charAt(x1)){
                    res = false;
                }
            }  
        }
        return res;
    }

    boolean esSufijo(String s1, String s2) {
        boolean res=true;
        if(s1.length()>s2.length()){
            res = false;
        }
        else{
            for(int x1=0;x1<s1.length();x1++){
                if (s1.charAt(s1.length()-x1-1)!=s2.charAt(s2.length()-x1-1)){
                    res = false;
                }
            }  
            }
        return res;
    }
}
