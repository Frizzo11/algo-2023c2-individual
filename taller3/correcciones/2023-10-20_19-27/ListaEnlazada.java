package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private class Nodo {
        T dato;
        Nodo siguiente;
        Nodo anterior;

        public Nodo(T dato) {
            this.dato = dato;
            this.siguiente = null;
            this.anterior = null;
        }
    }

    private Nodo primero;
    private Nodo ultimo;
    private int size;

    public ListaEnlazada(ListaEnlazada<T> lista) {
        this();
        Nodo actual = lista.primero;
        while (actual != null) {
            this.agregarAtras(actual.dato);
            actual = actual.siguiente;
        }
    }

    public ListaEnlazada() {
        primero = null;
        ultimo = null;
        size = 0;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        nuevoNodo.siguiente = primero;
        if (primero != null) {
            primero.anterior = nuevoNodo;
        }
        primero = nuevoNodo;
        if (ultimo == null) {
            ultimo = nuevoNodo;
        }
        size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        nuevoNodo.anterior = ultimo;
        if (ultimo != null) {
            ultimo.siguiente = nuevoNodo;
        }
        ultimo = nuevoNodo;
        if (primero == null) {
            primero = nuevoNodo;
        }
        size++;
    }

    public void eliminar(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Índice fuera de rango.");
        }
        Nodo actual = primero;
        for (int j = 0; j < i; j++) {
            actual = actual.siguiente;
        }
        if (actual.anterior != null) {
            actual.anterior.siguiente = actual.siguiente;
        } else {
            primero = actual.siguiente;
        }
        if (actual.siguiente != null) {
            actual.siguiente.anterior = actual.anterior;
        } else {
            ultimo = actual.anterior;
        }
        size--;
    }

    public T obtener(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Índice fuera de rango.");
        }
        Nodo actual;
        if (i < size / 2) {
            actual = primero;
            for (int j = 0; j < i; j++) {
                actual = actual.siguiente;
            }
        } else {
            actual = ultimo;
            for (int j = size - 1; j > i; j--) {
                actual = actual.anterior;
            }
        }
        return actual.dato;
    }

    public void modificarPosicion(int indice, T elem) {
        if (indice < 0 || indice >= size) {
            throw new IndexOutOfBoundsException("Índice fuera de rango.");
        }
        Nodo actual;
        if (indice < size / 2) {
            actual = primero;
            for (int i = 0; i < indice; i++) {
                actual = actual.siguiente;
            }
        } else {
            actual = ultimo;
            for (int i = size - 1; i > indice; i--) {
                actual = actual.anterior;
            }
        }
        actual.dato = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada<T>();
        Nodo actual = primero;
        while (actual != null) {
            copia.agregarAtras(actual.dato);
            actual = actual.siguiente;
        }
        return copia;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Nodo actual = primero;
        while (actual != null) {
            sb.append(actual.dato);
            if (actual.siguiente != null) {
                sb.append(", ");
            }
            actual = actual.siguiente;
        }
        sb.append("]");
        return sb.toString();
    }

    private class ListaIterador implements Iterador<T> {
        private Nodo actual = primero;
        private Nodo anterior = null;

        public boolean haySiguiente() {
            return actual != null;
        }

        public boolean hayAnterior() {
            return anterior != null;
        }

        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay siguiente elemento.");
            }
            T dato = actual.dato;
            anterior = actual;
            actual = actual.siguiente;
            return dato;
        }

        public T anterior() {
            if (!hayAnterior()) {
                throw new NoSuchElementException("No hay elemento anterior.");
            }
            T dato = anterior.dato;
            actual = anterior;
            anterior = anterior.anterior;
            return dato;
        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }
}


