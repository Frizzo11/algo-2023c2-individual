package aed;

class Debugging {
    boolean xor(boolean a, boolean b) { //listo
        return (a ^ b);
    }

    boolean iguales(int[] xs, int[] ys) { //listo
        boolean res = true;
        if (xs.length != ys.length){
            res = false;
            return res;
        }
        else{
            for (int i = 0; i < xs.length; i++) {
                if (xs[i] != ys[i]) {
                    res = false;
                }
            }
        }
        return res;

    }

    boolean ordenado(int[] xs) {
        boolean res = true;
        for (int i = 0; i < (xs.length - 1); i++) {
            if (xs[i] > xs[i+1]) {
                res = false;
            }
        }
        return res;
    }

    int maximo(int[] xs) { //listo
        int res = xs[0];
        for (int i = 0; i < xs.length; i++) {
            if (xs[i] > res) res = xs[i];
        }
        return res;
    }

    boolean todosPositivos(int[] xs) { //listo
        boolean res = true;
        for (int x : xs) {
            if (x > 0) {
                res = true;
            } else {
                if (xs.length == 1 && xs[0] == 0) {
                    res = true;
                    return res;
                }
                else {
                    res = false;
                    return res;
                }
            }
        }
        return res;
    }
}
